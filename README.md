# Devops Demo fundamentals

> This is practice for devops demo
> Gitlab-runner would be set up from out site
> When you commit a new code, the pipeline will be triggered and ran. After `test`, `build`, `tag`. This project will be deployed to domain `devops.wayarmy.net`


## Preparation: Register gitlab runner to this project

- Login to the ubuntu server, run the following command to setup the gitlab-runner:

```shell
# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

- Register the gitlab-runner with shell executor and `REGISTRATION_TOKEN_FROM_GITLAB` var will be received from CI/CD pages.

```
gitlab-runner register \
    --non-interactive \
    --url "https://gitlab.com" \
    --registration-token "GR1348941Jv2JjWnocLoPAiaFB5Nk" \
    --description "DevOps Fundamentals CI" \
    --executor "shell"
```


## STEP 1: Create your account

- Create your own account on [gitlab.com](https://gitlab.com) and [Docker hub](https://hub.docker.com/)
- I will send you the invitation to be a contributor of this project

## STEP 2: Contribute your code into this project

- *Step 2.1:* From the [Project site](https://gitlab.com/wayarmy/devops-fundamentals), click on the `Web IDE` tab
- *Step 2.2:* Create a new file with the name: `<your name>.html`
- *Step 2.3:* Input the following content into the `HTML` file that you've added:

```html
<html>
    <h1>DevOps Fundamentals</h1>
    My name is: <strong>{YOUR NAME}</strong>
</html>

```

- *Step 2.4:* Create commit (Create new branch with the name is your name and start a new merge request) with commit message

```
your name
```

## STEP 3: Code reviewer will review and merge your code into the branch master

## STEP 4: Verify your deployment

- Please access to the result by entering the URL on your browser:

```
http://devops.wayarmy.net/{yourname}.html
```
